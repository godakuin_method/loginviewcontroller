//
//  LoginViewController.swift
//  LoginViewController
//
//  Created by iga34 on 2019/12/29.
//  Copyright © 2019 godaikun_method. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UIPopoverPresentationControllerDelegate {

    ///UserIdを入力するtextField
    @IBOutlet weak var UserIdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    ///押下時にポップオーバーを表示させる。
    @IBAction func tappedPopover(_ sender: UIButton) {
        let viewController = TableViewController() //popoverで表示するViewController
        viewController.modalPresentationStyle = .popover
        viewController.preferredContentSize = CGSize(width: 200, height: 44*3)
        let presentationController = viewController.popoverPresentationController
        presentationController?.delegate = self
        presentationController?.permittedArrowDirections = .up
        presentationController?.sourceView = sender
        presentationController?.sourceRect = sender.bounds
        
        viewController.closure = { (viewController) in
          print("タップされた")
        }
        
        
        present(viewController, animated: true, completion: nil)
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController,
                                   traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

